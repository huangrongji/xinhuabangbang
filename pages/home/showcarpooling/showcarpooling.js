

Page({
  /**
   * 页面的初始数据
   */
  data: {
    showcarpoolingList: [{
          carpooling_place: '文冲',
          carpooling_toplace: '广州科学城',
          carpooling_start_date: '2020-06-02',
          carpooling_start_time: '18:00',
          carpooling_end_date: '2020-06-10',
          carpooling_end_time: '21:00',
          carpooling_content: '15一个马上走！'
        },
        { 
          carpooling_place: '文冲',
          carpooling_toplace: '广州科学城',
          carpooling_start_date: '2020-06-02',
          carpooling_start_time: '18:00',
          carpooling_end_date: '2020-06-10',
          carpooling_end_time: '21:00',
          carpooling_content: '15一个马上走！'
        },
        {
          carpooling_place: '文冲',
          carpooling_toplace: '广州科学城',
          carpooling_start_date: '2020-06-02',
          carpooling_start_time: '18:00',
          carpooling_end_date: '2020-06-10',
          carpooling_end_time: '21:00',
          carpooling_content: '15一个马上走！'
        }, ] //测试用数据
      },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this; //=====注意此处，要用that 指代this=====
    wx.request({
      url: '', //服务器地址
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      data: {
        carpooling_id: options.carpooling_id,
      },
      header: { // 设置请求的 header
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res);
        that.setData({ //======不能直接写this.setDate======
          showcarpoolingList: res.data, //在相应的wxml页面显示接收到的数据
        });
      },
      fail: function (res) {
        console.log('request fail');
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */

  /**
     * 页面加载完成
     */
  onReady: function () {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },



})